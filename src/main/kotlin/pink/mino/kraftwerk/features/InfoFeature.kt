package pink.mino.kraftwerk.features

import org.bukkit.Bukkit
import org.bukkit.scheduler.BukkitRunnable
import pink.mino.kraftwerk.config.ConfigOptionHandler
import pink.mino.kraftwerk.utils.Chat
import kotlin.random.Random

class InfoFeature : BukkitRunnable() {
    val prefix: String = "&aInfo&r&b »&r&f"
    private val announcements = listOf(
        Chat.colored("$prefix Apply for staff using &f/apply&7!"),
        Chat.colored("$prefix Want to host games on here? Apply for staff! &f/apply&7!"),
        //Chat.colored("$prefix Like our server @ &fnamemc.com/server/raimu.space&7!"),
        Chat.colored("$prefix Want more games hosted? Apply for staff @ &f/apply&7!"),
        Chat.colored("$prefix Join our Discord community to get notified about new matches, see Raimu-related content, and talk about Raimu! &fhttps://raimu.space/discord&7."),
        Chat.colored("$prefix View the store using &f/store&7!"),
        Chat.colored("$prefix View the server rules using &f/rules&7."),
        Chat.colored("$prefix View the health of other players using &f/health&7 or in the player list &6[Tab]&7!"),
        Chat.colored("$prefix View the stats of other players using &f/stats <player>&7!"),
        Chat.colored("$prefix View the UHC Configuration using &f/config&7!"),
        Chat.colored("$prefix Check which scenarios are active using &f/scenarios&7!"),
        Chat.colored("$prefix Don't know when the Loot Crate (or other) will spawn? Use &f/timers&7!"),
        Chat.colored("$prefix Wanna know who has the most kills in this match? Use &f/kt&7!"),
        Chat.colored("$prefix Message your team your mined ores using &f/pmminedores&7!"),
        Chat.colored("$prefix Message your team the ores you have using &f/pmores&7!"),
        //Chat.colored("$prefix Are you a content creator? Apply for media rank using &f/media&7!"),
        Chat.colored("$prefix Don't want to see someone's messages? Use &f/ignore&7!"),
        Chat.colored("$prefix Can't see? Use &f/fb&7 to enable night vision!"),
        Chat.colored("$prefix Team with other players using &f/team&7!"),
    )
    override fun run() {
        if (Bukkit.getOnlinePlayers().isNotEmpty()) {
            if (!ConfigOptionHandler.getOption("private")!!.enabled) {
                Bukkit.broadcastMessage(announcements[Random.nextInt(announcements.size)])
            }
        }
    }
}